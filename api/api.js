/**
 * third party libraries
 */
const bodyParser = require("body-parser");
const express = require("express");
const http = require("http");
const mapRoutes = require("express-routes-mapper");
const cors = require("cors");
const errorController = require("./controllers/ErrorController");

/**
 * server configuration
 */
const config = require("../config/");
// inclue db service
const dbService = require("./services/db.service");

// environment: development, production
const environment = process.env.NODE_ENV;

/**
 * express application
 */
const app = express();
server = http.Server(app);

const mappedOpenRoutes = mapRoutes(config.publicRoutes, "api/controllers/");

// start the DB service
const DB = dbService(environment, config.migrate).start();

// static folder goes here
// app.use(express.static(path.join(__dirname, 'public')));

// allow cross origin requests
// configure to only allow requests from certain origins
app.use(cors());

// parsing the request bodys
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// just to ckeck whether server is running or not
app.get("/", (req, res) => res.send("App is running..."));

const prefix = "/api";

// fill routes for express application
app.use(`${prefix}`, mappedOpenRoutes);

// just gives 404 error
app.use(errorController.get404);

server.listen(config.port, () => {
  if (environment !== "production" && environment !== "development") {
    console.error(
      `NODE_ENV is set to ${environment}, but only production and development are valid.`
    );
    process.exit(1);
  }
  return DB;
});
