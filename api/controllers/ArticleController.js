const Article = require("../models/Article");
const Comments = require("../models/Comments");

const ArticleController = () => {
  // Api for get all articles
  // @method GET
  // @return Array
  const index = async (req, res) => {
    try {
      let page = req.query.page;
      let perPage = req.query.perPage;

      if (!perPage) {
        perPage = 20; // if perPage is not define then take 20 default
      }

      if (page) {
        page = (page - 1) * perPage;
      } else {
        page = 0;
      }

      let articles = await Article.findAll({
        attributes: ["id", "title", "nickname", "createdAt"],
        order: [["id", "DESC"]],
        offset: page < 0 ? 0 : page,
        limit: parseInt(perPage),
      });

      if (articles.length === 0) {
        return res.status(400).json({
          errors: [
            {
              msg: "No articles found",
            },
          ],
        });
      }

      return res.json({
        message: "Data fetched successfully",
        articles,
      });
    } catch (err) {
      console.log(err);
      return res.status(400).json({ errors: [{ msg: err.message }] });
    }
  };

  // Api for get single articles
  // @method GET
  // @return article
  const getSingleArticle = async (req, res) => {
    try {
      const article = await Article.findOne({
        where: {
          id: req.params.article_id,
        },
      });
      if (!article) {
        return res.status(400).json({
          errors: [
            {
              msg: "Article not found",
            },
          ],
        });
      }

      return res.json({
        message: "Data fetched successfully",
        article,
      });
    } catch (err) {
      console.log(err);
      return res.status(400).json({ errors: [{ msg: err.message }] });
    }
  };

  // Api for create an articles
  // @method POST
  // @return article
  const create = async (req, res) => {
    try {
      const { nickname, title, content } = req.body;

      const article = await Article.create({
        nickname,
        title,
        content,
      });

      return res.json({
        message: "Article has been create successfully",
        article,
      });
    } catch (err) {
      console.log(err);
      return res.status(400).json({ errors: [{ msg: err.message }] });
    }
  };

  // Api for add comment on an articles
  // @method POST
  // @return comment
  const addCommentOnArticle = async (req, res) => {
    try {
      const { comment, parent_id, nickname } = req.body;

      const article = await Article.findOne({
        where: {
          id: req.params.article_id,
        },
      });
      if (!article) {
        return res.status(400).json({
          errors: [
            {
              msg: "Article not found",
            },
          ],
        });
      }

      // check if parent_id is given and its valid
      if (parent_id && parent_id !== 0) {
        const checkParent = await Article.findOne({
          where: {
            id: parent_id,
          },
        });
        if (!checkParent) {
          return res.status(400).json({
            errors: [
              {
                msg: "Invalid parent id found",
              },
            ],
          });
        }
      }

      // Fetch customer
      const commentFetch = await Comments.create({
        nickname,
        comment,
        parent_id: parent_id || 0,
        article_id: article.id,
      });

      return res.json({
        message: "Comment added successfully",
        comment: commentFetch,
      });
    } catch (err) {
      console.log(err);
      return res.status(400).json({ errors: [{ msg: err.message }] });
    }
  };

  // Api for get all comment of an articles
  // @method GET
  // @return Array
  const getAllComments = async (req, res) => {
    try {
      const article = await Article.findByPk(req.params.article_id);
      if (!article) {
        return res.status(400).json({
          errors: [
            {
              msg: "Article not found",
            },
          ],
        });
      }

      let page = req.query.page;
      let perPage = req.query.perPage;

      if (!perPage) {
        perPage = 10;
      }

      if (page) {
        page = (page - 1) * perPage;
      } else {
        page = 0;
      }

      // get all parent comments[Main comments]
      let listComments = await Comments.findAll({
        where: {
          article_id: req.params.article_id,
          parent_id: 0,
        },
        order: [["id", "DESC"]],
        offset: page < 0 ? 0 : page,
        limit: parseInt(perPage),
      });

      return res.json({
        message: "Data fetched successfully",
        comments: listComments,
      });
    } catch (err) {
      console.log(err);
      return res.status(400).json({ errors: [{ msg: err.message }] });
    }
  };

  // Api for get all sub comments of a comment
  // @method GET
  // @return Array
  const getAllSubComments = async (req, res) => {
    try {
      // check article exist
      const article = await Article.findByPk(req.params.article_id);
      if (!article) {
        return res.status(400).json({
          errors: [
            {
              msg: "Article not found",
            },
          ],
        });
      }

      // check main comment exist
      const mainComment = await Comments.findByPk(req.params.comment_id);
      if (!mainComment) {
        return res.status(400).json({
          errors: [
            {
              msg: "Comment not found",
            },
          ],
        });
      }

      let page = req.query.page;
      let perPage = req.query.perPage;

      if (!perPage) {
        perPage = 10;
      }

      if (page) {
        page = (page - 1) * perPage;
      } else {
        page = 0;
      }

      let listComments = await Comments.findAll({
        where: {
          parent_id: req.params.comment_id,
        },
        order: [["id", "DESC"]],
        offset: page < 0 ? 0 : page,
        limit: parseInt(perPage),
      });

      return res.json({
        message: "Data fetched successfully",
        comments: listComments,
      });
    } catch (err) {
      console.log(err);
      return res.status(400).json({ errors: [{ msg: err.message }] });
    }
  };

  return {
    index,
    getSingleArticle,
    create,
    getAllComments,
    addCommentOnArticle,
    getAllSubComments,
  };
};

module.exports = ArticleController;
