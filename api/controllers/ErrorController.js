exports.get404 = (req, res, next) => {
  res.status(404).json({
  	errors: [{
  		msg: 'The requested route was not found on this server, please check the url.',
  	}],
  });
  // res.status(404).render('404', { pageTitle: 'Page Not Found', path: '/404' });
};
