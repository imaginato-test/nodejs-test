const Sequelize = require("sequelize");
// include db service
const sequelize = require("../../config/database");

const tableName = "articles";

const Article = sequelize.define(
  "article",
  {
    nickname: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    title: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    content: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
  },
  { tableName, paranoid: true }
);

module.exports = Article;
