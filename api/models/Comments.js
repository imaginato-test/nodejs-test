const Sequelize = require("sequelize");
const sequelize = require("../../config/database");

const tableName = "comments";

const Comments = sequelize.define(
  "comments",
  {
    nickname: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    comment: Sequelize.TEXT,
    parent_id: {
      type: Sequelize.INTEGER,
      defaultValue: 0,
    },
  },
  { tableName, paranoid: true }
);

module.exports = Comments;
