const JWTService = require('../services/auth.service');
const User = require('../models/User');
const OuthAccessToken = require('../models/OuthAccessToken');

// usually: "Authorization: Bearer [token]" or "token: [token]"
module.exports = (req, res, next) => {
  let tokenToVerify;

  if (req.header('Authorization')) {
    const parts = req.header('Authorization').split(' ');

    if (parts.length === 2) {
      const scheme = parts[0];
      const credentials = parts[1];

      if (/^Bearer$/.test(scheme)) {
        tokenToVerify = credentials;
      } else {
        return res.status(401).json({
          errors: [{
            msg: req.polyglot.t('unauthorized'),
          }],
        });
      }
    } else {
      return res.status(401).json({
        errors: [{
          msg: req.polyglot.t('unauthorized'),
        }],
      });
    }
  }
  // else if (req.body.token) {
  //   tokenToVerify = req.body.token;
  //   delete req.query.token;
  // }
  else {
    return res.status(401).json({
      errors: [{
        msg: req.polyglot.t('unauthorized'),
      }],
    });
  }

  return JWTService().verify(tokenToVerify, (err, thisToken) => {
    if (err) {
      return res.status(401).json({
        errors: [{
          msg: req.polyglot.t('unauthorized'),
        }],
      });
    }

    req.token = thisToken;
    req.tokenToVerify = tokenToVerify;

    (async () => {
      const userCheckAuth = await OuthAccessToken.findOne({
        where: {
          user_id: req.token.id,
          token: tokenToVerify,
          revoke: 0,
        },
      });
      if (!userCheckAuth) {
        return res.status(401).json({
          errors: [{
            msg: req.polyglot.t('tokenHasBlackListed'),
          }],
        });
      }

      const user = await User.findByPk(req.token.id);
      if (!user) {
        return res.status(400).json({
          errors: [{
            msg: req.polyglot.t('userNotFound'),
          }],
        });
      }
      user.last_seen = Date.now();
      await user.save();

      return next();
    })();

    // return next();
  });
};
