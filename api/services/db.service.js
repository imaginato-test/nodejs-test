// load all models
const Article = require("../models/Article");
const Comments = require("../models/Comments");

// load db service
const database = require("../../config/database");

const dbService = (environment, migrate) => {
  const authenticateDB = () => database.authenticate();

  const dropDB = () => database.drop();

  const syncDB = () => database.sync();

  const createRelation = () =>
    new Promise((resolve) => {
      // Article-Comments Relationship start
      Article.hasMany(Comments, {
        foreignKey: "article_id",
      });
      Comments.belongsTo(Article, {
        foreignKey: "article_id",
      });
      // Article-Comments Relationship end
      resolve(true);
    });

  const successfulDBStart = () =>
    console.info(
      "connection to the database has been established successfully"
    );

  const errorDBStart = (err) =>
    console.info("unable to connect to the database:", err);

  const wrongEnvironment = () => {
    console.warn(
      `only development, staging, test and production are valid NODE_ENV variables but ${environment} is specified`
    );
    return process.exit(1);
  };

  const startMigrateTrue = async () => {
    try {
      await syncDB();
      successfulDBStart();
    } catch (err) {
      errorDBStart(err);
    }
  };

  const startMigrateFalse = async () => {
    try {
      await dropDB();
      await syncDB();
      successfulDBStart();
    } catch (err) {
      errorDBStart(err);
    }
  };

  const startDev = async () => {
    try {
      await authenticateDB();

      await createRelation();

      if (migrate) {
        return startMigrateTrue();
      }

      return startMigrateFalse();
    } catch (err) {
      return errorDBStart(err);
    }
  };

  const startProd = async () => {
    try {
      await authenticateDB();

      await createRelation();

      if (migrate) {
        return startMigrateTrue();
      }

      await startMigrateFalse();
    } catch (err) {
      errorDBStart(err);
    }
  };

  const start = async () => {
    switch (environment) {
      case "development":
        await startDev();
        break;
      case "production":
        await startProd();
        break;
      default:
        await wrongEnvironment();
    }
  };

  return {
    start,
  };
};

module.exports = dbService;
