// Development db details
const development = {
  database: "node-app",
  username: "root",
  password: "",
  host: "localhost",
  dialect: "mysql",
};

// Production db details
const production = {
  database: "node-app", // change with your DB
  username: "root", // change with your username
  password: "", // change with your password
  host: "localhost", // change with your host
  dialect: "mysql", // change with your db dialect
};

module.exports = {
  development,
  production,
};
