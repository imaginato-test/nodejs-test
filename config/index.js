const publicRoutes = require("./routes/publicRoutes");

const config = {
  migrate: true,
  publicRoutes,
  port: process.env.NODE_ENV === "production" ? "3001" : "3003",
};

module.exports = config;
