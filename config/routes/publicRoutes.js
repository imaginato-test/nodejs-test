const { body, validationResult } = require("express-validator");

// can be reused by many routes
const validate = (validations) => async (req, res, next) => {
  await Promise.all(validations.map((validation) => validation.run(req)));
  const errors = validationResult(req);
  if (errors.isEmpty()) {
    return next();
  }
  const extractedErrors = [];
  // errors.array().map(err => extractedErrors.push({ [err.param]:  }))
  errors.array().map((err) => extractedErrors.push({ msg: err.msg }));
  return res.status(400).json({
    errors: extractedErrors,
  });
};

const publicRoutes = {
  "GET /articles": "ArticleController.index",
  "GET /articles/:article_id": "ArticleController.getSingleArticle",
  "POST /articles": {
    path: "ArticleController.create",
    middlewares: [
      validate([
        body("nickname").not().isEmpty().withMessage("Nick name is required"),
        body("title").not().isEmpty().withMessage("Title is required"),
        body("content").not().isEmpty().withMessage("Content is required"),
      ]),
    ],
  },
  "GET /articles/:article_id/comments": "ArticleController.getAllComments",
  "POST /articles/:article_id/comments": {
    path: "ArticleController.addCommentOnArticle",
    middlewares: [
      validate([
        body("nickname").not().isEmpty().withMessage("Nick name is required"),
        body("comment").not().isEmpty().withMessage("Comment is required"),
      ]),
    ],
  },
  "GET /articles/:article_id/comments/:comment_id":
    "ArticleController.getAllSubComments",
};

module.exports = publicRoutes;
