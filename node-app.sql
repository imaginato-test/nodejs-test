-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 09, 2020 at 08:16 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `node-app`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deletedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `nickname`, `title`, `content`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(1, 'DKP', 'Lorem ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2020-12-09 06:12:37', '2020-12-09 06:12:37', NULL),
(2, 'DKP', 'Lorem ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2020-12-09 06:12:38', '2020-12-09 06:12:38', NULL),
(3, 'DKP', 'Lorem ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2020-12-09 06:12:39', '2020-12-09 06:12:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `article_id` int(11) DEFAULT NULL,
  `nickname` varchar(255) NOT NULL,
  `comment` text DEFAULT NULL,
  `parent_id` int(11) DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deletedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `article_id`, `nickname`, `comment`, `parent_id`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(1, 1, 'ABCD', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 0, '2020-12-09 06:25:35', '2020-12-09 06:25:35', NULL),
(2, 1, 'ABCD', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 0, '2020-12-09 06:25:51', '2020-12-09 06:25:51', NULL),
(3, 1, 'ABCD', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 0, '2020-12-09 06:25:52', '2020-12-09 06:25:52', NULL),
(4, 1, 'ABCD', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 0, '2020-12-09 06:25:52', '2020-12-09 06:25:52', NULL),
(5, 1, 'XYZ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 1, '2020-12-09 06:36:34', '2020-12-09 06:36:34', NULL),
(6, 1, 'XYZ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 1, '2020-12-09 06:36:36', '2020-12-09 06:36:36', NULL),
(7, 1, 'XYZ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 1, '2020-12-09 06:36:37', '2020-12-09 06:36:37', NULL),
(8, 1, 'PQR', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 2, '2020-12-09 06:36:47', '2020-12-09 06:36:47', NULL),
(9, 1, 'PQR', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 2, '2020-12-09 06:36:47', '2020-12-09 06:36:47', NULL),
(10, 1, 'KBC', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 0, '2020-12-09 06:38:53', '2020-12-09 06:38:53', NULL),
(11, 1, 'WWR', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 3, '2020-12-09 06:41:59', '2020-12-09 06:41:59', NULL),
(12, 1, 'KBC', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 0, '2020-12-09 07:00:16', '2020-12-09 07:00:16', NULL),
(13, 1, 'WWR', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 3, '2020-12-09 07:02:29', '2020-12-09 07:02:29', NULL),
(14, 1, 'KBC', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 0, '2020-12-09 07:02:37', '2020-12-09 07:02:37', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_id` (`article_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
